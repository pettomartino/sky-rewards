require 'ruby-enum'

module Enumerators
  class Reward
    include Ruby::Enum

    define :SPORTS, 'CHAMPIONS_LEAGUE_FINAL_TICKET'
    define :MUSIC, 'KARAOKE_PRO_MICROPHONE'
    define :MOVIES, 'PIRATES_OF_THE_CARIBBEAN_COLLECTION'
  end
end
