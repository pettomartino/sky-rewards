require 'ruby-enum'

module Enumerators
  class Eligibility
    include Ruby::Enum

    define :CUSTOMER_ELIGIBLE, 'CUSTOMER_ELIGIBLE'
    define :CUSTOMER_INELIGIBLE, 'CUSTOMER_INELIGIBLE'
  end
end
