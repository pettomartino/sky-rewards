require './enumerators/eligibility'
require './enumerators/reward'

class RewardsService

  def initialize(eligibility_service:)
    @eligibility_service = eligibility_service
  end

  def rewards(account_number, channel_subscriptions)
    return unless eligible?(account_number)

    available_reward_channels(channel_subscriptions)
  end

  private

  def eligible?(account_number)
    eligibility = Enumerators::Eligibility.key(@eligibility_service.eligibility(account_number))
    eligibility == :CUSTOMER_ELIGIBLE
  rescue
    nil
  end

  def available_reward_channels(channels)
    available_reward_channels = Enumerators::Reward.to_h
    channels.each_with_object([]) do |channel, result|
      reward = available_reward_channels[channel]
      result.push(reward) unless reward.nil?
    end
  end
end