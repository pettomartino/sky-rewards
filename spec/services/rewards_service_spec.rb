require 'spec_helper'
require './services/rewards_service'
require './enumerators/eligibility'

RSpec.describe RewardsService do

  let (:account_number) { 1234 }

  context 'Errors' do
    it 'returns InvalidAccountNumberError' do
      subject = RewardsService.new(eligibility_service: eligibility_service_fail('Invalid account number'))

      rewards = subject.rewards(account_number, [])

      expect(rewards).to be_nil
    end

    it 'returns InvalidAccountNumberError' do
      subject = RewardsService.new(eligibility_service: eligibility_service_fail('Technical failure exception'))

      rewards = subject.rewards(account_number, [])

      expect(rewards).to be_nil
    end
  end

  context 'CUSTOMER_INELIGIBLE' do
    it 'returns no rewards' do
      subject = RewardsService.new(eligibility_service: ineligible_eligibility_service)

      rewards = subject.rewards(account_number, [])

      expect(rewards).to be_nil
    end
  end

  context 'CUSTOMER_ELIGIBLE' do
    it 'returns not nil' do
      subject = RewardsService.new(eligibility_service: eligible_eligibility_service)

      rewards = subject.rewards(account_number, [])

      expect(rewards).not_to be_nil
    end

    it 'returns rewards for sports' do
      subject = RewardsService.new(eligibility_service: eligible_eligibility_service)
      channel_subscriptions = [:SPORTS]

      rewards = subject.rewards(account_number, channel_subscriptions)

      expect(rewards).to eq ['CHAMPIONS_LEAGUE_FINAL_TICKET']
    end

    it 'returns no rewards when channel does not give any reward' do
      subject = RewardsService.new(eligibility_service: eligible_eligibility_service)

      rewards = subject.rewards(1234, [:KIDS])

      expect(rewards).to eq []
    end

    it 'returns no rewards when channel does not give any reward' do
      subject = RewardsService.new(eligibility_service: eligible_eligibility_service)
      channel_subscriptions = [:KIDS]

      rewards = subject.rewards(account_number, channel_subscriptions)

      expect(rewards).to eq []
    end

    it 'returns all avaiable rewards for given array' do
      subject = RewardsService.new(eligibility_service: eligible_eligibility_service)
      channel_subscriptions = [:KIDS, :SPORTS, :MUSIC]

      rewards = subject.rewards(account_number, channel_subscriptions)

      expect(rewards).to eq ['CHAMPIONS_LEAGUE_FINAL_TICKET', 'KARAOKE_PRO_MICROPHONE']
    end
  end

  def eligibility_service_fail(message)
    eligibility_service = double('eligibility_service')
    allow(eligibility_service).to receive(:eligibility).and_raise(message)
    eligibility_service
  end

  def ineligible_eligibility_service
    eligibility_service(message: 'CUSTOMER_INELIGIBLE')
  end

  def eligible_eligibility_service
    eligibility_service(message: 'CUSTOMER_ELIGIBLE')
  end

  def eligibility_service(message:)
    eligibility_service = double('eligibility_service')
    allow(eligibility_service).to receive(:eligibility).and_return(message)
    eligibility_service
  end
end